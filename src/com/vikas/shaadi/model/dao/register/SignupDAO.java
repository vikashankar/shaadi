package com.vikas.shaadi.model.dao.register;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vikas.shaadi.dto.register.SignupDTO;

@Component
public class SignupDAO {
	
	@Autowired
	private SessionFactory factory;

	public SignupDAO() {
		System.out.println(this.getClass().getSimpleName() + "Created...");
	}

	public void save(SignupDTO dto) {
		System.out.println("calling save in dao , should use hibernate \t"
				+ dto);
		
		Session session = factory.openSession();
		Transaction tx=session.beginTransaction();

		try {
			session.save(dto);
			tx.commit();

		} catch (HibernateException he) {
			he.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}

}