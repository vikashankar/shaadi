package com.vikas.shaadi.model.service.register;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vikas.shaadi.dto.register.SignupDTO;
import com.vikas.shaadi.model.dao.register.SignupDAO;

@Component
public class SignupService {

	@Autowired
	private SignupDAO signupDAO;

	public SignupService() {
		System.out.println(this.getClass().getSimpleName() + "Created...");
	}

	public void create(SignupDTO dto) {
		System.out.println("calling create in service..." + dto);
		if (dto != null) {
			System.out.println("Valid data can call dao");
			signupDAO.save(dto);
			return;
		}
		System.out.println("invalid data , cannot call dao");

	}

}

