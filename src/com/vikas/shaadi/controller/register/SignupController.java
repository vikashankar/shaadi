package com.vikas.shaadi.controller.register;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vikas.shaadi.dto.register.SignupDTO;
import com.vikas.shaadi.model.service.register.SignupService;

@Component
@RequestMapping("/")
public class SignupController {

	@Autowired
	private SignupService signupService;

	public SignupController() {
		System.out.println(this.getClass().getSimpleName() + "Created...");
	}

	@RequestMapping(value = "/signup.do", method = RequestMethod.POST)
	public ModelAndView signup(SignupDTO dto) {
		System.out.println("calling signup \t" + dto);
		signupService.create(dto);

		ModelAndView modelAndView = new ModelAndView("/Shaadi.jsp");
		modelAndView.addObject("successMessage", "Is success , " + dto.getName());
		return modelAndView;
	}

}
